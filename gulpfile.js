var gulp = require("gulp");
var browserify = require("gulp-browserify");
var uglify = require("gulp-uglify");
var rename = require("gulp-rename");
var jshint = require("gulp-jshint");
gulp.task('lint',function() {
    return gulp.src('./client/**/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});
gulp.task('browserify', function(){
    return gulp.src('client/app.js')
        .pipe(browserify({
            insertGlobals: true
        }))
        .pipe(rename('build.js'))
        .pipe(gulp.dest('build'))
        .pipe(gulp.dest('public/js'))
});
gulp.task('copy', function() {
    return gulp.src('client/index.html')
        .pipe(gulp.dest('public'))
})
gulp.task('watch',function(){
    gulp.watch('client/**/*.js',['browserify']);
    gulp.watch('client/index.html',['copy'])
});