# -*- mode: ruby -*-
# vi: set ft=ruby :
# https://gist.github.com/bbaaxx/d88d433f402539fafc4e
box      = 'ubuntu/trusty64'
hostname = 'nodebox'
domain   = 'example.com'
ip       = '192.168.42.42'
ram      = '512'

$rootScript = <<SCRIPT
  echo "I am provisioning..."
  echo doing it as $USER
  cd /home/vagrant
  add-apt-repository ppa:git-core/ppa
  apt-get update
  apt-get install -y vim git-core curl
SCRIPT

$userScript = <<SCRIPT
  cd /home/vagrant
  wget -qO- https://raw.github.com/creationix/nvm/master/install.sh | sh
  export NVM_DIR="/home/vagrant/.nvm"
  [ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm
  nvm install 0.12
  nvm alias default 0.12
  npm install -g bower
SCRIPT

VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

  config.vm.box = box
  config.vm.hostname = hostname

  # Forwarding default ports for ember server and livereload
  config.vm.network :forwarded_port, guest: 4200, host: 4200, auto_correct: true
  config.vm.network :forwarded_port, guest: 35729, host: 35729, auto_correct: true

  config.vm.network "private_network", ip: "10.42.42.42"

  config.ssh.forward_agent = true

  config.vm.synced_folder ".", "/vagrant",
    owner: "vagrant", group: "vagrant"

  # Removes "stdin: is not a tty" annoyance as per
  # https://github.com/SocialGeeks/vagrant-openstack/commit/d3ea0695e64ea2e905a67c1b7e12d794a1a29b97
  config.ssh.shell = "bash -c 'BASH_ENV=/etc/profile exec bash'"

  config.vm.provider "virtualbox" do |vb|
    vb.customize  [
                    "modifyvm", :id,
                    "--memory", ram,
                  ]

    # Allow the creation of symlinks for nvm
    # http://blog.liip.ch/archive/2012/07/25/vagrant-and-node-js-quick-tip.html
    vb.customize ["setextradata", :id, "VBoxInternal2/SharedFoldersEnableSymlinksCreate/vagrant","1"]
  end

  # Shell provisioning.
  config.vm.provision "shell", inline: $rootScript
  config.vm.provision "shell", inline: $userScript, privileged: false

end
