var Dispatcher = require("../dispatcher");
module.exports = {
    createTest: function(test){
        Dispatcher.dispatch({
            type: 'TEST_ACTION',
            text: test
        });
    }
}