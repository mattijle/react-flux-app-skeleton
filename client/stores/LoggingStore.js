var Dispatcher = require('../dispatcher');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var _ = require('lodash');
var _log = [];
var LoggingStore = assign({},EventEmitter.prototype,{});
LoggingStore.dispatchToken = Dispatcher.register(function(action){
    //Maybe send actions to server aswell. But for debugging purposes we are just going to console.log them.
    _log.push(action);
    console.log(action);
});
module.exports = LoggingStore;